# DnD5e-AR
This module adds support for Arabic langauge FoundryVTT's DND5e game system.

## Installation
In the Add-On Modules option click on Install Module and place the following link in the field Manifest URL

https://gitlab.com/mr_aljabry/dnd5e-ar/-/raw/master/dnd5e/module.json

If this option does not work download the DnD5e-AR.zip file and extract its contents into the /resources/app/public/modules/ folder
Once this is done, enable the module in the settings of the world in which you intend to use it and then change the language in the settings.

# اللغة العربية لنظام DnD5e
هذه الوحدة تضيف دعم اللغة العربية لنظام لعبة DnD5e.

## التثبيت
في خيار الوحدات الملحقة إنقر على تثبيت الوحدة وضع الرابط التالي في خانة عنوان URL

https://gitlab.com/mr_aljabry/dnd5e-ar/-/raw/master/dnd5e/module.json

إذا لم يعمل هذا الخيار قم بتحميل ملف DnD5e-AR.zip واستخرج محتوياته في ملف /resources/app/public/modules/
متى ما تم هذا، قم بتفعيل الوحدة في اعدادات عالمك الذي ترغب في استخدامها فيه وقم بتغير اللغة في الاعدادات.
